const { Logging } = require("@google-cloud/logging");
const logging = new Logging();
const fs = require("fs");

query = `
  resource.type="gae_app"
  resource.labels.module_id="default"
  logName=("projects/omega-arg/logs/stdout" OR "projects/omega-arg/logs/stderr" OR "projects/omega-arg/logs/appengine.googleapis.com%2Frequest_log")
  (protoPayload.resource:amiright) OR (textPayload:bot)
`;

let log = [];

function callback(err, entries, nextQuery, apiResponse) {
  if (err) throw err;
  log = log.concat(entries);
  if (nextQuery) {
    // More results exist.
    console.log(`Getting another page... current length ${log.length}`);
    logging.getEntries(nextQuery, callback);
  } else {
    parsed = log.map(l => {
      if (l.protoPayload) {
        return JSON.stringify(l.protoPayload);
      } else {
        return JSON.stringify(l);
      }
    });
    fs.writeFile("log.json", `[${parsed.join(",")}]`, function(err) {
      if (err) throw err;
      console.log("complete");
    });
  }
}

logging.getEntries(
  {
    autoPaginate: false,
    filter: query,
    pageSize: 1000
  },
  callback
);
