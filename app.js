const express = require("express");
const session = require("express-session");
const _ = require("lodash");
const moment = require("moment");
const passport = require("passport");
const DiscordStrategy = require("passport-discord").Strategy;
const Datastore = require("@google-cloud/datastore");
const DatastoreStore = require("@google-cloud/connect-datastore")(session);
require("dotenv").config();

if (process.env.NODE_ENV === "production") {
  require("@google-cloud/debug-agent").start();
}

const bot = require("./bot");
const db = require("./db");

const app = express();
const PORT = process.env.PORT || 8080;
const URL =
  process.env.NODE_ENV === "production"
    ? "https://omega-arg.appspot.com"
    : `http://localhost:${PORT}`;
const scopes = ["identify"];
app.enable("trust proxy");
app.set("view engine", "pug");
app.locals._ = _;

// starting time to calculate times against
const STARTING_TIME = moment("2019-01-26T17:00:00-08:00");
// latest attempts from users (to spam protect)
const latestAttempt = {};

passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});
passport.use(
  new DiscordStrategy(
    {
      clientID: process.env.DISCORD_ID,
      clientSecret: process.env.DISCORD_SECRET,
      callbackURL: `${URL}/callback`,
      scope: scopes
    },
    (accessToken, refreshToken, profile, cb) => {
      process.nextTick(() => {
        return cb(null, profile);
      });
    }
  )
);
app.use(
  session({
    store: new DatastoreStore({
      dataset: Datastore({
        kind: "express-sessions"
      })
    }),
    secret: "retro is secretly omega",
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static("public"));

function checkAuth(req, res, next) {
  if (req.isAuthenticated()) return next();
  req.session.returnTo = req.originalUrl;
  res.redirect("/login");
}

app.get(
  "/login",
  passport.authenticate("discord", { scope: scopes }),
  (req, res) => {
    res.redirect("/");
  }
);

app.get(
  "/callback",
  passport.authenticate("discord", { failureRedirect: "/login" }),
  (req, res) => {
    res.redirect(req.session.returnTo || "/");
    delete req.session.returnTo;
  }
);

function timeTaken(date) {
  const milliseconds = moment(date).diff(STARTING_TIME);
  const d = moment.duration(milliseconds);
  const HH = String(Math.floor(d.asHours())).padStart(2, "0");
  const mm = String(d.minutes()).padStart(2, "0");
  const ss = String(d.seconds()).padStart(2, "0");
  const SSS = String(d.milliseconds()).padStart(3, "0");
  const string = `${HH}:${mm}:${ss}.${SSS}`;
  return string;
}

app.get("/", checkAuth, async (req, res) => {
  const gatesSolved = await db.gatesSolved(req.user);
  // find the last solved gate
  let maxGate = -1;
  if (gatesSolved.length > 0) maxGate = _.maxBy(gatesSolved, "gate").gate;
  let finished = gatesSolved.length >= 21;
  res.render("index", {
    user: req.user,
    started: STARTING_TIME.isBefore(),
    countdown: moment.duration(STARTING_TIME.diff()).humanize(true),
    gates: db.gates
      .filter(g => g.id <= (finished ? 21 : 20))
      .map(gate => {
        const copy = { ...gate };
        if (gate.id <= maxGate) {
          const solved = _.find(gatesSolved, ["gate", gate.id]);
          copy.timeTaken = timeTaken(solved.date);
          copy.solved = "SOLVED";
        } else if (gate.id === maxGate + 1) {
          copy.solved = "OPEN";
        } else {
          copy.solved = "LOCKED";
        }
        return copy;
      })
  });
});

app.get("/leaderboard", (req, res) => {
  both = ["Silox", "Yoshi"];
  giftCard = ["DED", "Yoshi"];
  discountCodes = [
    "DED",
    "DED",
    "DED",
    "Ayia",
    "Ayia",
    "Yoshi",
    "Retro",
    "Ashkowski",
    "Tooth",
    "LucHumphrey",
    "LucHumphrey",
    "AcousticPal",
    "DutChen",
    "DutChen",
    "DutChen",
    "Avery (SURPRISE! 21 is a lucky number today)"
  ];

  db.getLeaderboard()
    .then(users => {
      const promises = users.map(async user => {
        return {
          user: await bot.findUser(user.user),
          time: timeTaken(user.date),
        };
      });
      return Promise.all(promises);
    })
    .then(leaderboard =>
      res.render("leaderboard", {
        leaderboard,
        prizes: _.flatten([
          ["$100 CreatorINK Gift Card, courtesy of Ayia"],
          both.map(
            name =>
              `$50 CreatorINK Gift Card AND $28.99 coupon for Theory Wear, courtesy of ${name}`
          ),
          giftCard.map(name => `$50 CreatorINK Gift Card, courtesy of ${name}`),
          discountCodes.map(
            name => `$28.99 coupon for Theory Wear, courtesy of ${name}`
          )
        ])
      })
    );
});

app.get("/amiright", checkAuth, async (req, res) => {
  const password = decodeURIComponent(req.query.p);
  // only allow requests once every 10 seconds
  if (Date.now() - latestAttempt[req.user.id] < 10000) {
    res.redirect(`/fast?p=${password}`);
    return;
  }
  latestAttempt[req.user.id] = Date.now();

  // check password
  const gate = _.find(db.gates, g => {
    if (g.insensitive) {
      return g.password.toLowerCase() === password.toLowerCase();
    }
    return g.password === password;
  });
  if (!gate) {
    res.redirect("/wrong");
    return;
  }

  // check gates being solved in order
  const gatesSolved = await db.gatesSolved(req.user);
  const lastGate = _.maxBy(gatesSolved, "gate") || { gate: 0 };
  if (gate.id > lastGate.gate + 1) {
    console.log(
      `[web] ${req.user.username} tried to solve ${gate.id} before ${
        lastGate.gate
      }`
    );
    res.redirect("/wrong");
    return;
  }

  let firstSolve = false;
  if (!_.find(gatesSolved, ["gate", gate.id])) {
    bot.gatePassed(req.user, gate, STARTING_TIME.isBefore());
    firstSolve = true;
  }
  const next = _.find(db.gates, ["id", gate.id + 1]);
  res.render("password", { user: req.user, gate, firstSolve, next });
});

app.get("/wrong", (req, res) => res.render("wrong"));
app.get("/fast", (req, res) => res.render("fast", { password: req.query.p }));

app.listen(PORT, () => {
  console.log(`[web] Listening on port ${PORT}`);
});
if (!process.env.NO_DISCORD) bot.start();
