const fs = require("fs");
const _ = require("lodash/fp");
const yaml = require("yaml");
const Datastore = require("@google-cloud/datastore");

class DB {
  constructor() {
    // keys are {user.id}-{gate.id} to make it easy to get and set
    this.datastore = Datastore();

    const gatesFile = fs.readFileSync("./gates.yaml", "utf8");
    this.gates = yaml.parse(gatesFile).gates;
  }

  whitelist(users) {
    const entities = users.map(user =>
      this.gates.map(gate => ({
        key: this.datastore.key(["Status", `${user.id}-${gate.id}`]),
        data: {
          user: user.id,
          gate: gate.id,
          date: new Date()
        }
      }))
    );
    const flattened = _.flatten(entities);
    return this.datastore.upsert(flattened);
  }

  getGate(id) {
    return _.find(["id", id])(this.gates);
  }

  getStatus(query) {
    return this.datastore.runQuery(query).then(results => results[0]);
  }

  getAllStatuses() {
    const query = this.datastore.createQuery("Status").order("date");
    return this.getStatus(query).then(
      _.flow(
        // collect all with same user and order by number of gates solved
        _.groupBy("user"),
        _.orderBy(["length"], ["desc"]),
        // format output
        _.map(entry => ({
          user: entry[0].user,
          gates: _.sortBy("id")(entry.map(x => ({ id: +x.gate, date: x.date })))
        }))
      )
    );
  }

  getLeaderboard() {
    const query = this.datastore.createQuery("Status").filter("gate", ">", 20);
    return this.getStatus(query).then(
      _.flow(
        _.groupBy("user"),
        // filter out whitelists
        _.filter(["length", 1]),
        _.map(entry => ({
          user: entry[0].user,
          date: entry[0].date
        })),
        _.orderBy("date", "asc")
      )
    );
  }

  passGate(user, gate) {
    return this.datastore.save({
      key: this.datastore.key(["Status", `${user.id}-${gate.id}`]),
      data: {
        user: user.id,
        gate: gate.id,
        date: new Date()
      }
    });
  }

  gatesSolved(user) {
    const query = this.datastore
      .createQuery("Status")
      .filter("user", "=", user.id);
    return this.datastore.runQuery(query).then(results => results[0]);
  }

  reset() {
    return this.getStatus().then(results => {
      const keys = results.map(e => e[this.datastore.KEY]);
      return this.datastore.delete(keys);
    });
  }
}

const db = new DB();
module.exports = db;
