const Discord = require("discord.js");
const _ = require("lodash/fp");
const moment = require("moment");
const db = require("./db");
const client = new Discord.Client();

client.on("ready", () => {
  console.log(`[bot] tag: ${client.user.tag}!`);
});

client.on("error", err => {
  console.error(`[bot] error: ${err.message}!`);
});

function generateEmbed() {
  return db.getAllStatuses().then(results => {
    let message = "Nobody has solved anything yet.";
    let solveLists = [];
    if (results.length > 0) {
      total = 0;
      for (let gate = 21; gate > 0; gate--) {
        solvers = results.filter(
          entry => _.maxBy("id")(entry.gates).id === gate
        );
        if (solvers.length > 0) {
          fields = _.flow(
            _.map(e => `<@${e.user}>`),
            _.chunk(40),
            _.map.convert({ cap: false })((list, i, all) => ({
              name: `Gate ${gate} (${
                all.length > 0 && i === 0 ? solvers.length : "cont."
              })`,
              value: list.join(", ")
            }))
          )(solvers);
          solveLists = solveLists.concat(fields);
          total += solvers.length;
        }
      }
      message = `Total solvers: ${total}`;
    }
    // use rich embed so that the mentions don't actually ping anyone
    return {
      title: "Omega ARG Progress List",
      description: message,
      fields: solveLists
    };
  });
}

function updateLeaderboard() {
  generateEmbed().then(embed => {
    const channel = client.channels.get(process.env.DISCORD_TARGET_CHANNEL);
    channel
      .fetchMessage(process.env.DISCORD_TARGET_MESSAGE)
      .then(msg =>
        msg.edit(`Updated ${moment().format("HH:mm:ss [UTC]Z")}`, { embed })
      )
      .then(msg => {
        msg.channel.send("Leaderboard updated!");
        console.log("[bot] Updated leaderboard");
      });
  });
}

client.on("message", msg => {
  const command = msg.content.split(" ");
  switch (command[0]) {
    case "o!list":
      generateEmbed().then(embed => {
        if (command[1] === "new") return msg.channel.send({ embed });
        msg.channel.send(
          "Use the pinned message, or if you really want to spam the chat use `o!list new`."
        );
      });
      break;
    case "o!whitelist":
      if (msg.channel.id !== process.env.DISCORD_OMEGA_CHANNEL) {
        msg.channel.send(
          "Oops! This command can only be done from the Omega server!"
        );
        break;
      }
      const users = msg.mentions.users;
      if (users.length === 0) {
        msg.channel.send("Mention user(s) to mark all gates completed");
      }
      db.whitelist(users).then(() => {
        msg.channel.send(
          `Whitelisted: ${users.map(u => u.username).join(", ")}`
        );
      });
      break;
    case "o!reset":
      if (msg.channel.id !== process.env.DISCORD_OMEGA_CHANNEL) {
        msg.channel.send(
          "Oops! This command can only be done from the Omega server!"
        );
        break;
      }
      if (command[1] !== "iamverysureiwanttodothis") {
        msg.channel.send(
          "**THIS WILL DELETE ALL PROGESS** Retype this command but with `iamverysureiwanttodothis` afterwards."
        );
        break;
      }
      db.reset().then(() => {
        msg.channel.send("Successfully cleared progress of all players");
      });
      break;
    case "o!announcement":
      if (msg.channel.id !== process.env.DISCORD_OMEGA_CHANNEL) {
        msg.channel.send(
          "Oops! This command can only be done from the Omega server!"
        );
        break;
      }
      const announcement = msg.content.substring("o!announcement ".length);
      client.channels
        .get(process.env.DISCORD_TARGET_CHANNEL)
        .send(announcement);
      break;
  }
});

module.exports = {
  findUser: id => client.fetchUser(id),
  gatePassed: (user, gate, started) => {
    // insert into datastore
    db.passGate(user, gate)
      .then(() => db.getStatus())
      .then(results => {
        const channel = client.channels.get(
          started
            ? process.env.DISCORD_TARGET_CHANNEL
            : process.env.DISCORD_OMEGA_CHANNEL
        );
        const whitelist = _.flow(
          _.filter(["gate", "420"]),
          _.map("user")
        )(results);
        const whitelistedResults = _.filter(
          // search only results where user is not on whitelist
          entry => whitelist.indexOf(entry.user) === -1
        );
        // check if it's the first time gate has been solved
        const firstSolve =
          _.filter(["gate", gate.id])(whitelistedResults).length === 1;
        if (firstSolve) {
          // if won
          if (gate.id === 21) {
            channel.send(
              `<@&${process.env.DISCORD_TARGET_ROLE}> Send your congrats to <@${
                user.id
              }>, who has WON FIRST PLACE!!`
            );
            console.log(`[bot] [winner] ${user.username} won!`);
          } else {
            channel.send(
              `<@&${process.env.DISCORD_TARGET_ROLE}> <@${
                user.id
              }> is the first to pass Gate ${gate.id}, the ${
                gate.title
              } Member!`
            );
            console.log(
              `[bot] [first] ${user.username} solved ${gate.author}'s gate`
            );
          }
        } else {
          if (gate.id === 21) {
            channel.send(
              `<@${user.id}> has completed ALL THE GATES! Congrats!`
            );
            console.log(`[bot] [winner] ${user.username}`);
          } else {
            channel.send(
              `<@${user.id}> has passed Gate ${gate.id}, the ${
                gate.title
              } Member!`
            );
            console.log(`[bot] ${user.username} solved ${gate.author}'s gate`);
          }
        }
        updateLeaderboard();
      });
  },
  start: async () => {
    await client.login(process.env.DISCORD_BOT_KEY);
    // updateLeaderboard();
  }
};
